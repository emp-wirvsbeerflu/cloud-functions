import * as functions from 'firebase-functions';
import got from 'got';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const triggerDueDateCleanup = functions.region('europe-west1').pubsub.schedule('every 4 hours').onRun(async (_context) => {
    await Promise.all([
    got('https://staging-shoppinglist-dot-emp-wirvsbeerflu.appspot.com/shoppinglist/checkDueDate'),
    got('https://shoppinglist-dot-emp-wirvsbeerflu.appspot.com/shoppinglist/checkDueDate'),
    got('https://staging-shoppinglist-dot-emp-wirvsbeerflu.appspot.com/user/purgeCache'),
    got('https://shoppinglist-dot-emp-wirvsbeerflu.appspot.com/user/purgeCache'),
    ])
})
export const triggerWrongListCleanup = functions.region('europe-west1').pubsub.schedule('every 24 hours').onRun(async (_context) => {
    await Promise.all([
    got('https://staging-shoppinglist-dot-emp-wirvsbeerflu.appspot.com/shoppinglist/fixWrongLists'),
    ])
})
